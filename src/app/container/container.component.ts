import { Component, OnInit, ElementRef, Renderer2, AfterViewInit } from '@angular/core';
import { ApiService } from '../api.service';
import { User } from '../model/user.model';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit{

  users: User[] = [];
  cardsEl = [1 , 2, 3, 4, 5, 6, 7, 8, 9, 10];
  answer = false;
  left = false;
  active = true;
  updatedActive = 0

  constructor(private userService: ApiService,
    private el: ElementRef, private renderer: Renderer2 ) {

  }


ngOnInit(): void {
this.userService.getAllUsers()
.subscribe(data => {
  this.users = data;
})

}



toggleAnswer() {
  this.answer = !this.answer;
}

toggleLeft() {
  this.left = !this.left;
}

toggleActive(bool: boolean) {
  this.active = bool;
}

reciebeActive(active: number) {
 this.updatedActive = active;
}


}

import { User } from './../model/user.model';
import { Component, Input, Output, EventEmitter  } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  @Output() left = new EventEmitter()
  @Output() active = new EventEmitter<boolean>()
  @Output() updateActive = new EventEmitter<number>()
  @Input() users;
  @Input() cardsEl;

  currentActive = 0;

// showOnlyActive() {

//

//     const activeEl = document.getElementById(`${this.currentActive}`) as HTMLElement

//     activeEl.classList.add('left')


//   }

 next() {

  for (let i = 0; i < this.cardsEl.length; i++) {
    const el = document.getElementById(`${i}`) as HTMLElement
    el.classList.remove('active')
  }


  const prevEl = document.getElementById(`${this.currentActive}`) as HTMLElement


  prevEl.className = 'card left'

  if (this.currentActive < this.cardsEl.length - 1) {
    this.currentActive = this.currentActive + 1
    this.updateActive.emit(this.currentActive)
  }

  if (this.currentActive > this.cardsEl.length - 1) {
      this.currentActive = this.cardsEl.length - 1
  }


  const activeEl = document.getElementById(`${this.currentActive}`) as HTMLElement

  activeEl.className = 'card active'

 }

 prev() {
  for (let i = 0; i < this.cardsEl.length; i++) {
    const el = document.getElementById(`${i}`) as HTMLElement
    el.classList.remove('active')
  }
  const activeEl = document.getElementById(`${this.currentActive}`) as HTMLElement

  activeEl.className = 'card right'

  if (this.currentActive > 0)
  this.currentActive = this.currentActive - 1
  this.updateActive.emit(this.currentActive)

  if (this.currentActive < 0) {
    this.currentActive = 0
  }
  const prevEl = document.getElementById(`${this.currentActive}`) as HTMLElement

  prevEl.className = 'card active'
 }





}
